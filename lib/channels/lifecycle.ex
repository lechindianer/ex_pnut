defmodule ExPnut.Channels.Lifecycle do
  import ExPnut.Helper.HTTP

  @moduledoc """
  Note that channels will appear "unread" if there is no stream marker for the user+channel, or if the marker's `id` is
  lower than the most recent message in the channel.

  For details on how to use channels for private messaging, look at [How To Private Message](https://pnut.io/docs/api/how-to/channels-pm).

  Endpoints:
  - Create a channel
  - Update a channel
  - Delete a channel

  [https://pnut.io/docs/api/resources/channels/lifecycle](https://pnut.io/docs/api/resources/channels/lifecycle)
  """

  @doc """
  Create a channel.

  `type` is necessary. ACLs must be valid. By default, all of it is editable after creating the channel! Be sure to
  restrict editing if you want to prevent it, and read [How to ACL](https://pnut.io/docs/api/how-to/channels-acl).

  ## Examples

      iex> acl = %{
              "full" => %{
                "user_ids" => ["@lechindianer", 1],
                "immutable" => false,
              },
              "read" => %{
                "any_user" => true,
              },
          }
      iex> ExPnut.Channels.Lifecycle.create(client, "com.example.site", acl)
  """
  def create(client, type, acl) do
    payload = %{
      "acl" => acl,
      "type" => type
    }

    payload_jsonified = ExPnut.Helper.JSON.jsonify(payload)

    post(client, "/channels", payload_jsonified)
  end

  @doc """
  Update a channel.

  Channels are only editable if they have an ACL that is not `immutable`. Full-access users and the
  owner can edit a channel, but only the owner can change who is a full-access user.

  ## Examples

      iex> acl = %{
              "full" => %{
                "user_ids" => ["@lechindianer", 1],
                "immutable" => false,
              },
              "read" => %{
                "any_user" => true,
              },
          }
      iex> ExPnut.Channels.Lifecycle.update(client, 42, "com.example.site", acl)
  """

  def update(client, channel_id, type, acl) do
    payload = %{
      "acl" => acl,
      "type" => type
    }

    payload_jsonified = ExPnut.Helper.JSON.jsonify(payload)

    patch(client, "/channels/#{channel_id}", payload_jsonified)
  end

  @doc """
  Deactivate a channel. Only the owner of a channel can deactivate it. Deactivating a channel removes all
  subscriptions to the channel, but does nothing to the contents. It is irreversible. `io.pnut.core.pm`-type channels
  cannot be deactivated.

    ## Examples

      iex> ExPnut.Channels.Lifecycle.delete_channel(client, 42)
  """
  def delete_channel(client, channel_id) do
    delete(client, "/channels/#{channel_id}")
  end
end
