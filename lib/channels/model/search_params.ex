defmodule ExPnut.Channels.SearchParams do
  @moduledoc """
  Channels search params

  ## Parameters
  - order: One of `activity` (most recent message), `id` (most recently created), or `popularity` (how many messages
    have been made). Default is by activity.
  - q: Basic text string searched for in names and descriptions in a channel's `io.pnut.core.chat-settings` raw.
  - categories: Comma-separated list of: fun, lifestyle, profession, language, community, tech, event, general. Taken
    from `io.pnut.core.chat-settings` raw.
  - channel_types: Comma-separated list of channel types to include.
  - raw_types: Comma-separated list of attached raw types. Any matches returned.
  - exclude_channel_types: Comma-separated list of channel types to exclude.
  - is_private: If true, only include private channels.
  - is_public: If true, only include public-readable channels.
  - owner_id: Channels owned by the included user ID.

  [https://pnut.io/docs/api/resources/channels/search](https://pnut.io/docs/api/resources/channels/search)
  """

  @enforce_keys [:q]

  defstruct [
    :q,
    :categories,
    :channel_types,
    :raw_types,
    :exclude_channel_types,
    :is_private,
    :is_public,
    :owner_id,
    order: "activity"
  ]
end
