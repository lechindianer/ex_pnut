defmodule ExPnut.Channels do
  import ExPnut.Helper.HTTP

  @moduledoc """
  Channel object bundling all data related to a channel like recent messages, message count, etc.
  """

  @doc """
  Retrieve a list of channels the authenticated user has muted.

  ## Examples

      iex> ExPnut.Channels.Muting.get_channels(client)
  """
  def get_muted_channels(client) do
    get(client, "/users/me/channels/muted")
  end

  @doc """
  Mute subscriptions for a channel.

  Muting a channel prevents other users from being able to auto-subscribe you to that channel.
  Muting unsubscribes, if you were subscribed.

  ## Examples

      iex> ExPnut.Channels.Subscribing.mute(client, 42)
  """
  def mute(client, channel_id) do
    put(client, "/channels/#{channel_id}/mute", [])
  end

  @doc """
  Delete a subscription mute for a channel.

  ## Examples

      iex> ExPnut.Channels.Subscribing.unmute(client, 42)
  """
  def unmute(client, channel_id) do
    delete(client, "/channels/#{channel_id}/mute")
  end

  @doc """
  Retrieve a list of channels filtered by the given criteria.

  ## Examples

      iex> search_params = %ExPnut.Channels.SearchParams{q: "lechindianer"}
      iex> ExPnut.Channels.Search.search(client, search_params)
  """
  def search(client, %ExPnut.Channels.SearchParams{} = search_params) do
    params =
      search_params
      |> Map.from_struct()
      |> Enum.filter(fn {_, v} -> v !== nil end)
      |> URI.encode_query()

    get(client, "/channels/search?#{params}")
  end

  @doc """
  Retrieve a list of channels the authenticated user is subscribed to.

  ## Examples

      iex> ExPnut.Channels.Subscribing.get_channels(client)
  """
  def get_subscribed_channels(client) do
    get(client, "/users/me/channels/subscribed")
  end

  @doc """
  Retrieve a list of users subscribed to a channel.

  ## Examples

      iex> ExPnut.Channels.Subscribing.get_subscribers(client, 42)
  """
  def get_subscribers(client, channel_id) do
    get(client, "/channels/#{channel_id}/subscribers")
  end

  @doc """
  Subscribe to updates from a channel.

  Subscribed channels act like an "inbox" of channels ordered by their most recent messages.
  Subscribing unmutes it, if you were muting it.

  ## Examples

      iex> ExPnut.Channels.Subscribing.subscribe(client, 42)
  """
  def subscribe(client, channel_id) do
    put(client, "/channels/#{channel_id}/subscribe", [])
  end

  @doc """
  Delete a subscription for a channel.

  Unsubscribing also deletes any existing stream marker for the channel.

  ## Examples

      iex> ExPnut.Channels.Subscribing.unsubscribe(client, 42)
  """
  def unsubscribe(client, channel_id) do
    delete(client, "/channels/#{channel_id}/subscribe")
  end

  @doc """
  Retrieve a channel object.

  To only retrieve channels by certain types, include them in a comma-separated list in the query parameter like so:
  `channel_types=io.pnut.core.pm,com.example.site`.

  ## Examples

      iex> ExPnut.Channels.Lookup.get_channel(client, 42)
  """
  def get_channel(client, channel_id) do
    get(client, "/channels/#{channel_id}")
  end

  @doc """
  Retrieve a list of specified channel objects.

  To only retrieve channels by certain types, include them in a comma-separated list in the query parameter like so:
  `channel_types=io.pnut.core.pm,com.example.site`.

  Only returns the first 200 found.

  ## Examples

      iex> ExPnut.Channels.Lookup.get_channels(client, [23, 42, 666])
  """
  def get_channels(client, channel_ids) do
    channels = Enum.join(channel_ids, ",")
    get(client, "/channels?#{channels}")
  end

  @doc """
  Retrieve a list of channels created by the authenticated user.

  Returns a list of channels.

  ## Examples

      iex> ExPnut.Channels.Lookup.get_my_channels(client)
  """
  def get_my_channels(client) do
    get(client, "/users/me/channels")
  end

  @doc """
  Retrieve a Private Message channel for a set of users, if one exists.

  ## Parameters
  - user_ids: Comma-separated list of User IDs or @-usernames. The authenticated user can be included or excluded.

  ## Examples

    iex> ExPnut.Channels.Lookup.get_existing_pm(client, [1, 29, "@blumenkraft"])
  """
  def get_existing_pm(client, user_ids) do
    ids = Enum.join(user_ids, ",")
    ExPnut.Helper.HTTP.get_channel(client, "/users/me/channels/existing_pm?ids=#{ids}")
  end

  @doc """
  Retrieve the number of unread private messages for the authenticated user.

  ## Examples

    iex> ExPnut.Channels.Lookup.get_unread_pm(client)
  """
  def get_unread_pm(client) do
    get(client, "/users/me/channels/num_unread/pm")
  end

  @doc """
  Mark all unread private messages as read for the authenticated user.

  ## Examples

    iex> ExPnut.Channels.Lookup.mark_all_read_pm(client)
  """
  def mark_all_read_pm(client) do
    delete(client, "/users/me/channels/num_unread/pm")
  end
end
