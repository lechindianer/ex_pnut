defmodule ExPnut.Posts.SearchParams do
  @moduledoc """
  This struct encapsulates all of the data necessary to search for a pnut post.

  [https://pnut.io/docs/resources/posts/search](https://pnut.io/docs/resources/posts/search)
  """

  @enforce_keys [:q]

  defstruct [
    :q,
    :client_id,
    :created_after,
    :created_before,
    :creator_id,
    :file_id,
    :file_kinds,
    :has_mentions,
    :is_directed,
    :is_nsfw,
    :is_reply,
    :is_revised,
    :leading_mentions,
    :mentions,
    :poll_id,
    :raw_types,
    :reply_to,
    :tags,
    :thread_id,
    :url_domains,
    :urls,
    :user_types,
    order: "relevance"
  ]

  @typedoc """
  - `q`: List of words included in posts.
  - `client_id`: Only include posts created by this client ID.
  - `created_after`: ISO 8601-formatted timestamp after which posts were created.
  - `created_before`: ISO 8601-formatted timestamp before which posts were created.
  - `creator_id`: Only include posts created by this user ID.
  - `file_id`: Matches with this file attached.
  - `file_kinds`: Comma-separated list of oEmbed-attached file types (`video`, `audio`, `image`, `other`).
  - `has_mentions`: 1 or 0 to include posts with or without mentions. Excludes other mentions filters below.
  - `is_directed`: If 1, only include [directed post](https://pnut.io/docs/implementation/entities#leading-mentions).
  - `is_nsfw`: If 0, does not include NSFW posts.
  - `is_reply`: 1 or 0 to include messages that are or are not replies.
  - `is_revised`: If 1, only include revised posts.
  - `leading_mentions`: Comma-separated list of mentions at the beginning of a post. Any matches returned.
  - `mentions`: Comma-separated list of mentions. Any matches returned.
  - `poll_id`: Matches with this poll attached.
  - `raw_types`: Comma-separated list of attached raw types. Any matches returned.
  - `reply_to`: Only include posts replying to this post.
  - `tags`: Comma-separated list of tags. Any matches returned.
  - `thread_id`: Only include posts in this thread.
  - `url_domains`: Comma-separated list of domains. Any matches returned. Do not include `http://` or `www` in front
  of domain.
  - `urls`: Comma-separated list of URLs. Any matches returned.
  - `user_types`: Comma-separated list of user types of: human, feed, bot.
  - `order`: One of id or relevance. Default is by relevance.
  """
  @type t :: %__MODULE__{
          q: String.t(),
          client_id: String.t(),
          created_after: String.t(),
          created_before: String.t(),
          creator_id: String.t(),
          file_id: String.t(),
          file_kinds: String.t(),
          has_mentions: 0 | 1,
          is_directed: 0 | 1,
          is_nsfw: 0 | 1,
          is_reply: 0 | 1,
          is_revised: 0 | 1,
          leading_mentions: String.t(),
          mentions: String.t(),
          poll_id: String.t(),
          raw_types: String.t(),
          reply_to: String.t(),
          tags: String.t(),
          thread_id: String.t(),
          url_domains: String.t(),
          urls: String.t(),
          user_types: String.t(),
          order: String.t()
        }
end
