defmodule ExPnut.Posts.PostParams do
  @moduledoc """
  Any endpoint that returns post objects can be subject to these parameters.

  [https://pnut.io/docs/resources/posts}(https://pnut.io/docs/resources/posts)
  """

  defstruct include_deleted: 1,
            include_client: 1,
            include_counts: 1,
            include_html: 1,
            include_post_html: 1,
            include_bookmarked_by: 0,
            include_reposted_by: 0,
            include_directed_posts: 1,
            include_mention_posts: 1,
            include_copy_mentions: 1,
            include_replies: 1,
            include_muted: 0,
            include_raw: 0,
            include_post_raw: 0

  @typedoc """
  - `include_deleted`: Include deleted posts. Defaults to true.
  - `include_client`: Include the client object with the post. Defaults to true.
  - `include_counts`: Include the post's counts. Also affects any included user object. Defaults to true.
  - `include_html`: Should the post and user `html` field be included alongside the `text` field in the response
  objects? Defaults to true.
  - `include_post_html`: Should the post `html` field be included alongside the `text` field in the response objects?
  Defaults to true. Note that `include_html` takes priority if present.
  - `include_bookmarked_by`: Include `bookmarked_by`: a sampled list of users who bookmarked the post. Defaults to
  false.
  - `include_reposted_by`: Include `reposted_by`: a sampled list of users who reposted the post. Defaults to false.
  - `include_directed_posts`: Include posts with "leading mentions" of users you do not follow. Not applicable to all
  post streams. Defaults to true.
  - `include_mention_posts`: If false, do not include posts with mentions.
  - `include_copy_mentions`: Include "copy mentions" in the `/users/{user_id}/mentions` endpoint. Defaults to true.
  - `include_replies`: If false, do not include posts replying to other posts.
  - `include_muted`: Include posts from users you have muted. Defaults to false.
  - `include_raw`: Include [raw](https://pnut.io/docs/implementation/raw) on all objects. Defaults to false.
  - `include_post_raw`: Include [raw](https://pnut.io/docs/implementation/raw) on all post objects. Defaults to false.
  """
  @type t :: %__MODULE__{
          include_deleted: 0 | 1,
          include_client: 0 | 1,
          include_counts: 0 | 1,
          include_html: 0 | 1,
          include_post_html: 0 | 1,
          include_bookmarked_by: 0 | 1,
          include_reposted_by: 0 | 1,
          include_directed_posts: 0 | 1,
          include_mention_posts: 0 | 1,
          include_copy_mentions: 0 | 1,
          include_replies: 0 | 1,
          include_muted: 0 | 1,
          include_raw: 0 | 1,
          include_post_raw: 0 | 1
        }
end
