defmodule ExPnut.Post do
  import ExPnut.Helper.HTTP
  alias ExPnut.Posts.NewPost
  alias ExPnut.Posts.InteractionParams
  alias ExPnut.Posts.PostParams
  alias ExPnut.Posts.SearchParams

  @moduledoc """
  Post object bundling all data related to a post like content, , follower status, images, etc.

  [https://pnut.io/docs/resources/posts](https://pnut.io/docs/resources/posts)
  """

  defstruct [
    :content,
    :counts,
    :created_at,
    :id,
    :is_deleted,
    :is_nsfw,
    :is_revised,
    :raw,
    :reply_to,
    :repost_of,
    :revision,
    :source,
    :thread_id,
    :user,
    :user_id,
    :you_bookmarked,
    :you_reposted
  ]

  @typedoc """
  - `content`: NOTE: Not included if the post has been deleted.
    - `entities`: Rich text information for this post. See the
      [Entities](https://pnut.io/docs/implementation/entities) documentation.
    - `html	`: Server-generated annotated HTML rendering of post text.
    - `text`: User supplied text of the post. All Unicode characters allowed. Maximum length 256 characters. The
       maximum length can be retrieved from the Configuration endpoint.
  - `counts`:
    - `bookmarks`: The number of users who have bookmarked this post.
    - `replies`: The number of posts created in reply to this post.
    - `reposts`: The number of users who have reposted this post.
    - `threads`: The number of threads created in reply to this or other children of this post.
  - `created_at`: The time at which the post was created in ISO 8601 format; `YYYY-MM-DDTHH:MM:SSZ`.
  - `id`: Primary identifier for a post. This will be an integer, but it is always expressed as a string to avoid
  limitations with the way JavaScript integers are expressed. This id space is unique to Post objects. There can be a
  Post and User with the same ID; no relation is implied.
  - `is_deleted`: Only set if true. Post is deleted. `content` will not be set.
  - `is_nsfw`: Only set if true. User marked the post as "Not Safe For Work".
  - `is_revised`: Only set if true. Post has been revised. Looking up the revised posts will return a result.
  - `raw`: The raw items attached to this object. Only included if query parameter specified.
  - `reply_to`: Optional id of the post this post is replying to.
  - `repost_of`: Optional embedded post object being reposted.
  - `revision`: Only set if post is a "previous" version of a post. (i.e., from the `/posts/{post_id}/revisions`
  endpoint).
  - `source`:
    - `id`: The public client id of the API consumer ("app") that created this post.
    - `name`: Description of the API consumer that created this post.
    - `url`: Link provided by the API consumer that created this post.
  - `thread_id`: The id of the post at the root of the thread that this post is a part of. If `thread_id==id` then
  this property does not guarantee that the thread has > 1 post. Please see `replies` count.
  - `user`: This is an embedded [User](https://pnut.io/docs/resources/users) object. Note: In certain cases (e.g.,
  when a user account has been deleted), this key may be omitted.
  - `user_id`: Primary identifier for the user who created the channel. This is only included if the `user` above is
  omitted.
  - `you_bookmarked`: (Optional) True if authenticated user bookmarked the post.
  - `you_reposted`: (Optional) True if authenticated user reposted the post.
  """
  @type t :: %__MODULE__{
          content: %{
            entities: String.t(),
            html: String.t(),
            text: String.t()
          },
          counts: %{
            id: String.t(),
            name: String.t()
          },
          created_at: integer(),
          id: integer(),
          is_deleted: 0 | 1,
          is_nsfw: 0 | 1,
          is_revised: 0 | 1,
          raw: map(),
          reply_to: String.t(),
          repost_of: %ExPnut.Post{},
          revision: integer(),
          source: %{
            id: String.t(),
            name: String.t(),
            url: String.t()
          },
          thread_id: String.t(),
          user: %ExPnut.User{},
          user_id: String.t(),
          you_bookmarked: 0 | 1,
          you_reposted: 0 | 1
        }

  @doc """
  Report a post for abuse.

  These are the current reasons that will be honored for reporting:

  - "account_type": posting in a behavior counter to the purposes of account types
  - "nsfw": unflagged mature material according to the community guidelines
  - "soliciting": unwelcome soliciting
  - "user_abuse": use of the API or network to abuse another user

  ## Parameters

  - post_id: ID of the post to report.
  - reason: One of: "account_type", "nsfw", "soliciting", "user_abuse".
  """
  def report(client, post_id, reason, %PostParams{} = post_params \\ %PostParams{}) do
    reason = {:multipart, [{"reason", reason}]}

    post(client, "/posts/#{post_id}/report", reason, post_params)
  end

  def search(
        client,
        %SearchParams{} = search_params \\ %SearchParams{q: ""},
        %PostParams{} = post_params \\ %PostParams{}
      ) do
    filtered_search_params =
      search_params
      |> Map.from_struct()
      |> Map.to_list()
      |> Enum.filter(fn {_, value} -> value !== nil end)

    params =
      filtered_search_params
      |> Enum.map_join(fn {key, value} -> "#{key}=#{value}" end, "&")

    get(client, "/posts/search?#{params}", post_params)
  end

  @doc """
  The authenticated user's stream of posts from their followers and themself.
  """
  def me(client, %PostParams{} = post_params \\ %PostParams{}) do
    get(client, "/posts/streams/me", post_params)
  end

  @doc """
  A combined Personal Stream including the authenticated user's mentions.
  """
  def unified(client, %PostParams{} = post_params \\ %PostParams{}) do
    get(client, "/posts/streams/unified", post_params)
  end

  @doc """
  Posts mentioning the specified user.
  """
  def mentions(client, user_id, %PostParams{} = post_params \\ %PostParams{}) do
    get(client, "/users/#{user_id}/mentions", post_params)
  end

  @doc """
  Posts created by the specified user.

  If a user looks up a user they blocked or muted, the posts will still be retrieved.
  """
  def posts(client, user_id, %PostParams{} = post_params \\ %PostParams{}) do
    get(client, "/users/#{user_id}/posts", post_params)
  end

  @doc """
  A stream of all users' public posts.
  """
  def global(client, %PostParams{} = post_params \\ %PostParams{}) do
    get(client, "/posts/streams/global", post_params)
  end

  @doc """
  A stream of all posts that include the specified tag.
  """
  def tag(client, tag, %PostParams{} = post_params \\ %PostParams{}) do
    get(client, "/posts/tag/#{tag}", post_params)
  end

  @doc """
  Retrieve posts within a thread. Threads are separated by what root post all posts below it have replied to.
  """
  def get_thread(client, post_id, %PostParams{} = post_params \\ %PostParams{}) do
    get(client, "/posts/#{post_id}/thread", post_params)
  end

  @doc """
  Retrieve a post object.
  """
  def get_post(client, post_id, %PostParams{} = post_params \\ %PostParams{}) do
    get(client, "/posts/#{post_id}", post_params)
  end

  @doc """
  Retrieve a list of specified post objects. Only retrieves the first 200 found.
  """
  def get_posts(client, post_ids, %PostParams{} = post_params \\ %PostParams{}) do
    post_ids_query_param = Enum.join(post_ids, ",")

    get(client, "/posts?ids=#{post_ids_query_param}", post_params)
  end

  @doc """
  Retrieve a list of previous versions of a post, not including the most recent. Currently a post can only have one
  previous version.

  Revisions returned will have revision as a String number indicating which version of the post it is. Revisions start
  at "0".
  """
  def get_revisions(client, post_id, %PostParams{} = post_params \\ %PostParams{}) do
    get(client, "/posts/#{post_id}/revisions", post_params)
  end

  @doc """
  Create a post.

  On creation, you can automatically update the "personal" stream marker to the post's ID by including update_marker=1
  in the query string.

  Posts from the same human- or feed-type user cannot contain the same text within 120 seconds.

  An application/json Content-Type is preferred over form. Normal links and markdown links are parsed by the server
  by default.
  """
  def create_post(client, %NewPost{} = new_post, %PostParams{} = post_params \\ %PostParams{}) do
    post_jsonified = ExPnut.Helper.JSON.jsonify(new_post)

    post(client, "/posts", post_jsonified, post_params)
  end

  @doc """
  Edit or "revise" a post.

  - Can only be done within 300 seconds of the original post's creation
  - Can only be done once to a post
  - Must contain the same entities that were in the original post (Positions can change. Links can be formatted in any
  way, but the URLs have to be the same)

  Once a revision has been made, the original post can still be retrieved from the Revisions endpoint.

  Reposts made before the revision will continue to point at the original post.
  """
  def revise_post(
        client,
        postId,
        %NewPost{} = new_post,
        %PostParams{} = post_params \\ %PostParams{}
      ) do
    post_jsonified = ExPnut.Helper.JSON.jsonify(new_post)

    put_json(client, "/posts/#{postId}", post_jsonified, post_params)
  end

  @doc """
  Delete a post.
  """
  def delete_post(client, postId) do
    delete(client, "/posts/#{postId}")
  end

  @doc """
  Retrieve actions executed against a post.
  """
  def get_interactions(
        client,
        post_id,
        %InteractionParams{} = interaction_params \\ %InteractionParams{},
        %PostParams{} = post_params \\ %PostParams{}
      ) do
    interaction_params =
      interaction_params
      |> ExPnut.Helper.UrlParams.build()
      |> Enum.map_join(fn {key, value} -> "#{key}=#{value}" end, "&")

    get(client, "/posts/#{post_id}/interactions?#{interaction_params}", post_params)
  end

  @doc """
  Retrieve a list of explore streams.
  """
  def explore(client, %PostParams{} = post_params \\ %PostParams{}) do
    get(client, "/posts/streams/explore", post_params)
  end

  @doc """
  Retrieve a list of posts in an explore stream.

  Slug can be of the following values: "conversations", "photos", "trending" and "missed_conversations"
  """
  def explore_slug(client, slug, %PostParams{} = post_params \\ %PostParams{}) do
    get(client, "/posts/streams/explore/#{slug}", post_params)
  end

  @doc """
  Retrieve a list of bookmarks made by the specified user.

  Returned posts may include a note string field if looking up bookmarks made by the authorized user.
  """
  def get_bookmarks(client, user_id, %PostParams{} = post_params \\ %PostParams{}) do
    get(client, "/users/#{user_id}/bookmarks", post_params)
  end

  @doc """
  Bookmark a post.
  """
  def bookmark(client, post_id, %PostParams{} = post_params \\ %PostParams{}) do
    put_json(client, "/posts/#{post_id}/bookmark", [], post_params)
  end

  @doc """
  Bookmark a post.
  """
  def bookmark_with_note(client, post_id, note, %PostParams{} = post_params \\ %PostParams{}) do
    note_jsonified = ExPnut.Helper.JSON.jsonify(note)

    case String.length(note) > 128 do
      true -> {:error, :note_text_too_long}
      false -> put_json(client, "/posts/#{post_id}/bookmark", note_jsonified, post_params)
    end
  end

  @doc """
  Delete a bookmark.
  """
  def delete_bookmark(client, post_id) do
    delete(client, "/posts/#{post_id}/bookmark")
  end

  @doc """
  Repost another post. The repost will show up in followers' streams if they have not seen another repost of the same
  within the last week, and if the reposted post is not in their recent stream. It is created in its own thread, not
  the thread of the original post. This increments a user's post count.

  Reposting is a special action. Reposts act as complete posts in themselves, with the "original" post embedded as an
  additional object. Unlike normal posts, actions (reply, repost, bookmark) cannot be executed against a repost.
  """
  def repost(client, post_id, %PostParams{} = post_params \\ %PostParams{}) do
    put(client, "/posts/#{post_id}/repost", post_id, post_params)
  end

  @doc """
  Delete a repost. The actual repost is completely deleted; it does not leave behind a thread or deleted post to look
  up.

  Users can also delete their own reposts even if they no longer have access to the original reposted post (e.g.,
  they were blocked). In that case, the returned post in the data field is simply {"id":POST_ID,"you_reposted":false}.
  """
  def delete_repost(client, post_id) do
    delete(client, "/posts/#{post_id}/repost")
  end
end
