defmodule ExPnut.User.SearchParams do
  @moduledoc """
  This struct encapsulates all of the data necessary to search for a pnut user.

  [https://pnut.io/docs/api/resources/users/search](https://pnut.io/docs/api/resources/users/search)
  """

  @enforce_keys [:q]

  defstruct [
    :q,
    :created_after,
    :created_before,
    :is_follower,
    :is_following,
    :locale,
    :timezone,
    :user_types,
    order: "relevance"
  ]

  @typedoc """
  - `q`: List of words included in user profiles or names.
  - `created_after`: ISO 8601-formatted timestamp after which users were created.
  - `created_before`: ISO 8601-formatted timestamp before which users were created.
  - `is_follower`: Whether to include users who follow you. Requires authentication.
  - `is_following`: Whether to include users who you are following. Requires authentication.
  - `locale`: [Valid user locale](https://pnut.io/docs/resources/users#locales) e.g., `en_US`.
  - `timezone`: [Valid user timezone](https://pnut.io/docs/resources/users#timezones) e.g., `America/Chicago`.
  - `user_types`: Comma-separated list of user types of: human, feed, bot.
  - `order`: One of id or relevance. Default is by relevance when a q value is set.
  """
  @type t :: %__MODULE__{
          q: String.t(),
          created_after: String.t(),
          created_before: String.t(),
          is_follower: true | false,
          is_following: true | false,
          locale: String.t(),
          timezone: String.t(),
          user_types: String.t(),
          order: String.t()
        }
end
