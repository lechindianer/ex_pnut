defmodule ExPnut.User.UserParams do
  @moduledoc """
  Any endpoint that returns user objects (including any that return post objects, message objects, etc.) can be
  subject to these parameters.

  [https://pnut.io/docs/api/resources/users](https://pnut.io/docs/api/resources/users)
  """

  defstruct include_html: 1,
            include_user_html: 1,
            include_counts: 1,
            include_user: 1,
            include_presence: 0,
            include_raw: 0,
            include_user_raw: 0

  @typedoc """
  - `include_html`: Should the post and user `html` field be included alongside the `text` field in the response
  objects? Defaults to `true`.
  - `include_user_html`: Should the user `html` field be included alongside the `text` field in the response objects?
  Defaults to `true`. Note that `include_html` takes priority if present.
  - `include_counts`: Include the user's counts. Defaults to true.
  - `include_user`: Return the user as their complete object, or only as (string) ID if false. Defaults to true.
  - `include_presence`: Include the user's current presence. Defaults to false.
  - `include_raw`: Include [raw](https://pnut.io/docs/implementation/raw) on all objects. Defaults to false.
  - `include_user_raw`: Include [raw](https://pnut.io/docs/implementation/raw) on all user objects. Defaults to false.
  """
  @type t :: %__MODULE__{
          include_html: 0 | 1,
          include_user_html: 0 | 1,
          include_counts: 0 | 1,
          include_user: 0 | 1,
          include_presence: 0 | 1,
          include_raw: 0 | 1,
          include_user_raw: 0 | 1
        }
end
