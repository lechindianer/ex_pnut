defmodule ExPnut.User do
  import ExPnut.Helper.HTTP
  import ExPnut.Helper.JSON
  alias ExPnut.User.UserParams
  alias ExPnut.User.SearchParams

  @moduledoc """
  User object bundling all data related to a user like username, id, follower status, images, etc.

  ## General User Identifiers
  Unless otherwise specified, user identifiers can be any of the following:

  - User ID (`1`)
  - @username (`@33mhz`)
  - "me", when authenticated

  User IDs are preferred where convenient, because they take less effort and will never change (unlike usernames).

  Referring to usernames is not case-sensitive, but usernames will be returned from the API with the casing specified
  by the user -- so comparisons should normalize them before comparing.

  [https://pnut.io/docs/resources/users](https://pnut.io/docs/resources/users)
  """

  defstruct [
    :badge,
    :content,
    :counts,
    :created_at,
    :follows_you,
    :id,
    :locale,
    :name,
    :timezone,
    :type,
    :username,
    :you_blocked,
    :you_can_follow,
    :you_follow,
    :you_muted,
    :verified,
    :raw
  ]

  @typedoc """
  - `badge`: Optional. Badges are earned, currently only for supporting pnut.io.
  - `content`:
    - `avatar_image`:
      - `is_default`: Whether or not the user's avatar is the original identicon generated for their username, or an
        image they uploaded.
      - `height`: Original height of the image.
      - `url`: URL linking to the current avatar image.
      - `width`: Original width of the image.
    ,
    - `cover_image`:
      - `is_default`: Whether or not the user's avatar is the original identicon generated for their username, or an
        image they uploaded.
      - `height`: Original height of the image.
      - `url`: URL linking to the current avatar image.
      - `width`: Original width of the image.
    - `entities`: Rich text information for this user. See the [Entities](https://pnut.io/docs/implementation/entities)
    documentation.
    - `html`: Server-generated annotated HTML rendering of user text.
    - `markdown_text`: `text`, with the original markdown links preserved. Only included when looking up the
      authenticated user's profile or `GET /token`.
    - `text`: User supplied text of the user. All Unicode characters allowed. Maximum length 256 characters. The
      maximum length can be retrieved from the Configuration endpoint.
  - `counts`:
    - `bookmarks`: Number of bookmarks this user has saved.
    - `clients`: Number of public clients this user has created.
    - `followers`: Number of users following this user.
    - `followers`: Number of users this user is following.
    - `posts`: Number of posts created by this user.
  - `created_at`: The time at which the User was created in ISO 8601 format `YYYY-MM-DDTHH:MM:SSZ`.
  - `follows_you`: Whether or not this user follows you.
  - `id`: Primary identifier for a user. This will be an integer, but it is always expressed as a string to avoid
  limitations with the way JavaScript integers are expressed. This id space is unique to User objects. There can be a
  Post and User with the same ID; no relation is implied.
  - `locale`: User locale in ISO format.
  - `name`: Optional user-supplied name. All Unicode characters allowed. Maximum length 50 characters. Be sure to
  escape if necessary.
  - `timezone`: User timezone in tzinfo format.
  - `type`: human, feed, or bot. See [Account Types](https://pnut.io/about/account-types) to be sure of the
  implications.
  - `username`: Case sensitive. 20 characters, may only contain a-z, 0-9 and underscore.
  - `you_blocked`: Whether or not you blocked this user.
  - `you_can_follow`: Whether or not you can follow this user -- not taking into account `you_follow`.
  - `you_follow`: Whether or not you follow this user.
  - `you_muted`: Whether or not you muted this user.
  - `verified`:
    - `domain`: Optional domain verified to be owned by the user.
    - `url`: Optional URL verified to be owned by the user.
  - `raw`: The raw items attached to this object. Only included if query parameter specified.
  """
  @type t :: %__MODULE__{
          badge: %{
            id: String.t(),
            name: String.t()
          },
          content: %{
            avatar_image: %{
              is_default: 0 | 1,
              height: integer(),
              url: String.t(),
              width: integer()
            },
            cover_image: %{
              is_default: 0 | 1,
              height: integer(),
              url: String.t(),
              width: integer()
            },
            entities: map(),
            html: String.t(),
            markdown_text: String.t(),
            text: String.t()
          },
          counts: %{
            bookmarks: integer(),
            clients: integer(),
            followers: integer(),
            following: integer(),
            posts: integer()
          },
          created_at: String.t(),
          follows_you: 0 | 1,
          id: String.t(),
          locale: String.t(),
          name: String.t(),
          timezone: String.t(),
          type: String.t(),
          username: String.t(),
          you_blocked: 0 | 1,
          you_can_follow: 0 | 1,
          you_follow: 0 | 1,
          you_muted: 0 | 1,
          verified: %{
            domain: String.t(),
            url: String.t()
          },
          raw: map()
        }

  @doc """
  Retrieve a list of users filtered by the given criteria `ExPnut.User.SearchParams`

  ## Parameters

  - client: `ExPnut.Client`
  - search_params: `ExPnut.User.SearchParams`
  - user_params: `ExPnut.User.UserParams`
  """
  @spec search(ExPnut.Client.t(), SearchParams.t()) :: list(ExPnut.Users.User)
  def search(
        client,
        %SearchParams{} = search_params,
        %UserParams{} = user_params \\ %UserParams{}
      ) do
    filtered_search_params =
      search_params
      |> Map.from_struct()
      |> Map.to_list()
      |> Enum.filter(fn {_, value} -> value !== nil end)

    params =
      filtered_search_params
      |> Enum.map_join(fn {key, value} -> "#{key}=#{value}" end, "&")

    get(client, "/users/search?#{params}", user_params)
  end

  @doc """
  Retrieve a user object.

  ## Parameters

  - `user_id`: User ID or username with "@" symbol of the user to retrieve.

  ## Examples

      iex> ExPnut.User.get_user(client, "@lechindianer")
  """
  def get_user(client, user_id, %UserParams{} = user_params \\ %UserParams{}) do
    get(client, "/users/#{user_id}", user_params)
  end

  @doc """
  Retrieve a list of specified user objects. Only retrieves the first 200 found.

  ## Parameters

  - `user_ids`: List of user IDs or username.

  ## Examples

      iex> ExPnut.User.get_users(client, [1, "@lechindianer"])
  """
  def get_users(client, user_ids, %UserParams{} = user_params \\ %UserParams{}) do
    user_ids = Enum.join(user_ids, ",")

    get(client, "/users?ids=#{user_ids}", user_params)
  end

  @doc """
  Retrieve a list of all user IDs that authorize the requesting app. It is not paginated.

  Requires an app token.
  """
  def get_my_users(client, %UserParams{} = user_params \\ %UserParams{}) do
    get(client, "/apps/me/users/ids", user_params)
  end

  @doc """
  Retrieve a list of all user token objects that authorize the requesting app. Not currently paginated.

  Requires an app token.
  """
  def get_my_tokens(client, %UserParams{} = user_params \\ %UserParams{}) do
    get(client, "/apps/me/users/tokens", user_params)
  end

  @doc """
  Retrieve a list of muted users. Users may only see their own list of muted users.

  ## Examples

      iex> ExPnut.User.get_muted(client)
  """
  def get_muted(client, %UserParams{} = user_params \\ %UserParams{}) do
    get(client, "/users/me/muted", user_params)
  end

  @doc """
  Mute a user. Muted users will not appear in future requests.

  ## Examples

      iex> ExPnut.User.mute(client, "@spammerUser123")
  """
  def mute(client, user_id, %UserParams{} = user_params \\ %UserParams{}) do
    put(client, "/users/#{user_id}/mute", [user_id], user_params)
  end

  @doc """
  Unmute a user.

  ## Examples

      iex> ExPnut.User.unmute(client, "@spammerUser123")
  """
  def unmute(client, user_id, %UserParams{} = user_params \\ %UserParams{}) do
    delete(client, "/users/#{user_id}/mute", user_params)
  end

  @doc """
  Replaces the authenticated user's profile. Anything not included is removed.

  Requires a user object with "application/json" Content-Type. "timezone" and "locale" are required. Not including
  "name" or "text" will remove them from the profile.

  "text" must be a child key of "content".

  Returns the updated user object.
  """
  def update_profile(client, user_profile, %UserParams{} = user_params \\ %UserParams{}) do
    user_profile = jsonify(user_profile)

    put_json(client, "/users/me", user_profile, user_params)
  end

  @doc """
  Updates only specified parts of the authenticated user's profile.

  Requires a user object with "application/json" content type. "name", "text", "timezone", and "locale" are current
  options.

  "text" must be a child key of "content".

  Returns the updated user object.
  """
  def patch_user(client, user_profile, %UserParams{} = user_params \\ %UserParams{}) do
    user_profile = jsonify(user_profile)

    patch(client, "/users/me", user_profile, user_params)
  end

  @doc """
  Get the user's avatar.

  ## Parameters

  - user_id: ID of the user whose avatar to retrieve.

  ## Examples

      iex> ExPnut.User.get_avatar(client, "@lechindianer")
      iex> ExPnut.User.get_avatar_scaled_width(client, "@lechindianer", [width: 42])
      iex> ExPnut.User.get_avatar(client, "@lechindianer", [height: 23])
  """
  def get_avatar(client, user_id, %UserParams{} = user_params \\ %UserParams{}) do
    params = ExPnut.Helper.UrlParams.build(user_params)

    get_user_avatar(client, user_id, params)
  end

  def get_avatar(
        client,
        user_id,
        [height: height],
        user_params
      ) do
    params = ExPnut.Helper.UrlParams.build(user_params) ++ [h: height]

    get_user_avatar(client, user_id, params)
  end

  def get_avatar(
        client,
        user_id,
        [width: width],
        user_params
      ) do
    params = ExPnut.Helper.UrlParams.build(user_params) ++ [w: width]

    get_user_avatar(client, user_id, params)
  end

  defp get_user_avatar(client, user_id, params) do
    url = "/users/#{user_id}/avatar"

    headers = ExPnut.Helper.HTTP.default_headers(client)

    "#{client.endpoint}#{url}"
    |> HTTPoison.get!(headers, params: params, follow_redirect: true)
    |> Map.get(:body)
  end

  @doc """
  Uploads a new avatar image for the authenticated user.

  The uploaded image will be cropped to square and must be smaller than 2MiB.

  Can specify "Content-Type" of "multipart/form-data", with the file as key "avatar". "Content-Length" header must
  also be set.

  ## Examples

      iex> ExPnut.User.set_avatar(client, [url: "https://lechindianer.de/img/rss.png"])

      iex> avatar = get my new image here
      iex> ExPnut.User.set_avatar(client, [avatar: avatar])
  """
  def set_avatar(client, options, user_params \\ %UserParams{})

  def set_avatar(client, [avatar: avatar], user_params) do
    # FIXME: this doesn't work yet
    post(client, "/users/me/avatar", {:file, avatar}, user_params)
  end

  def set_avatar(client, [url: url], user_params) do
    # FIXME: this doesn't work yet
    post(client, "/users/me/avatar", {:form, [from_url: url]}, user_params)
  end

  @doc """
  This endpoint will return an HTTP 302 redirect to the user’s current cover image. It will include any query string
  parameters you pass to the endpoint.

  ## Parameters

  - `user_id`: ID of the user whose cover image to retrieve.

  ## Examples

      iex> ExPnut.User.get_cover(client, "@lechindianer")
      iex> ExPnut.User.get_cover(client, "@lechindianer", [width: 42])
      iex> ExPnut.User.get_cover(client, "@lechindianer", [height: 42])
  """
  def get_cover(client, user_id, _, user_params \\ %UserParams{})

  def get_cover(client, user_id, [height: height], user_params) do
    params = ExPnut.Helper.UrlParams.build(user_params) ++ [h: height]

    get_user_cover(client, user_id, params)
  end

  def get_cover(client, user_id, [width: width], user_params) do
    params = ExPnut.Helper.UrlParams.build(user_params) ++ [w: width]

    get_user_cover(client, user_id, params)
  end

  def get_cover(client, user_id) do
    params =
      %UserParams{}
      |> ExPnut.Helper.UrlParams.build()

    get_user_cover(client, user_id, params)
  end

  defp get_user_cover(client, user_id, params) do
    url = "/users/#{user_id}/cover"

    headers = ExPnut.Helper.HTTP.default_headers(client)

    "#{client.endpoint}#{url}"
    |> HTTPoison.get!(headers, params: params, follow_redirect: true)
    |> Map.get(:body)
  end

  @doc """
  Uploads a new cover image for the authenticated user.

  The uploaded image must be smaller than 4MiB, with a width of at least 960px and height of at least 223px.

  If the width / height ratio is less than 2 to 1, the height will be cropped to height / 4.3. A 10000x10000px image
  would be cropped to 10000x2326px.

  Can specify "Content-Type" of "application/json" with the key "{from_url}", or a "Content-Type" of
  "multipart/form-data", with the file as key "avatar". "Content-Length" header must also be set.
  """
  def set_cover(client, cover, %UserParams{} = user_params \\ %UserParams{}) do
    post(client, "/users/me/avatar", cover, user_params)
  end

  @doc """
  Retrieve a list of user objects that the specified user is following.
  """
  def get_following(client, user_id, %UserParams{} = user_params \\ %UserParams{}) do
    get(client, "/users/#{user_id}/following", user_params)
  end

  @doc """
  Retrieve a list of user objects that are following the specified user.
  """
  def get_followers(client, user_id, %UserParams{} = user_params \\ %UserParams{}) do
    get(client, "/users/#{user_id}/followers", user_params)
  end

  @doc """
  Follow a user.
  """
  def follow(client, user_id, %UserParams{} = user_params \\ %UserParams{}) do
    put(client, "/users/#{user_id}/follow", [user_id], user_params)
  end

  @doc """
  Unfollow a user.
  """
  def unfollow(client, user_id) do
    delete(client, "/users/#{user_id}/follow")
  end

  @doc """
  Retrieve a list of blocked users. Users may only see their own list of blocked users.
  """
  def get_blocked(client, user_id, %UserParams{} = user_params \\ %UserParams{}) do
    get(client, "/users/#{user_id}/blocked", user_params)
  end

  @doc """
  Block a user. Blocked users will not show up in future requests, the same as if they were muted. Blocked users also
  cannot retrieve this authorized user in their requests. Can do so even if the other user is blocking you (but will
  only return an ID of the blocked user).
  """
  def block(client, user_id, %UserParams{} = user_params \\ %UserParams{}) do
    put(client, "/users/#{user_id}/block", user_id, user_params)
  end

  @doc """
  Unblock a user. Can do so even if the other user is blocking you (but will only return an ID of the blocked user).
  """
  def unblock(client, user_id) do
    delete(client, "/users/#{user_id}/block")
  end

  @doc """
  Retrieve a badge object.
  """
  def get_badge(client, badge_id, %UserParams{} = user_params \\ %UserParams{}) do
    get(client, "/badges/#{badge_id}", user_params)
  end

  @doc """
  Retrieve a list of all badges. It is not paginated.
  """
  def get_badges(client, %UserParams{} = user_params \\ %UserParams{}) do
    get(client, "/badges", user_params)
  end

  @doc """
  Retrieve a list of all badges for the authenticated user. It is not paginated.
  """
  def get_my_badges(client, %UserParams{} = user_params \\ %UserParams{}) do
    get(client, "/users/me/badges", user_params)
  end
end
