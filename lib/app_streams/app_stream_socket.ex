defmodule ExPnut.AppStreamSocket do
  use WebSockex
  require Logger

  @every_40_seconds 40 * 1000

  @moduledoc false

  @doc """
  Create an app stream socket connection

  ## Parameters

  - map with 2 keys: `token` and `key`:
    - **token**: Your authenticated app token provided by `ExPnut.Auth.get_app_access_token\\0`
    - **key**: The app stream key provided by `ExPnut.AppStreams.create_stream\\2`

  ## Examples
    iex> ExPnut.AppStreamSocket.start_link(${:token => "my_app_token", :key => "my_app_stream_key" })
  """
  def start_link(%{:token => token, :key => key}) do
    connection_url = "wss://stream.pnut.io/v0/app?access_token=#{token}&key=#{key}"

    WebSockex.start_link(connection_url, __MODULE__, :fake_state)
  end

  def handle_connect(_conn, state) do
    Logger.info("Connected!")
    send_keepalive()
    {:ok, state}
  end

  def handle_frame({:text, "Close the things!" = msg}, :fake_state) do
    Logger.info("Received Message: #{msg}")
    {:close, :fake_state}
  end

  def handle_frame({:text, msg}, :fake_state) do
    Logger.info("Received Message: #{msg}")
    {:ok, :fake_state}
  end

  def handle_info(:keep_alive, state) do
    Logger.debug("Keep alive")
    send_keepalive()
    {:reply, {:text, "still alive"}, state}
  end

  def handle_disconnect(%{reason: {:local, reason}}, state) do
    Logger.info("Local close with reason: #{inspect(reason)}")
    {:ok, state}
  end

  def handle_disconnect(disconnect_map, state) do
    super(disconnect_map, state)
  end

  defp send_keepalive() do
    Process.send_after(self(), :keep_alive, @every_40_seconds)
  end
end
