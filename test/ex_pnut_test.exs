defmodule ExPnut.ClientTest do
  use ExUnit.Case
  doctest ExPnut.Client

  test "creates a client" do
    endpoint = Application.fetch_env!(:ex_pnut, :endpoint)
    expected_client = %ExPnut.Client{auth: nil, endpoint: endpoint}

    assert ExPnut.Client.new() == expected_client
  end
end
