defmodule ExPnut.MixProject do
  use Mix.Project

  def project do
    [
      app: :ex_pnut,
      version: "0.1.0",
      elixir: "~> 1.13",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      name: "Ex.Pnut",
      source_url: "https://gitlab.com/lechindianer/ex_pnut"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.6.3", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.1.0", only: [:dev], runtime: false},
      {:ex_doc, "~> 0.28.2", only: :dev, runtime: false},
      {:httpoison, "~> 1.8.0"},
      {:jason, "~> 1.3.0"},
      {:websockex, "~> 0.4.3"}
    ]
  end

  defp description() do
    "ExPnut is an Elixir wrapper for the pnut API."
  end

  defp package() do
    [
      licenses: ["ISC"],
      links: %{"GitHub" => "https://gitlab.com/lechindianer/ex_pnut/"}
    ]
  end
end
